"use strict";
// Адаптивное меню
document.querySelector('.header__burger').onclick = function () {
    document.querySelector('.header__burger').classList.toggle('active');
    document.querySelector('.header__menu').classList.toggle('active');
    document.querySelector('body').classList.toggle('lock');
};
// Вывод выбранной даты
const month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',];
const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday',]
document.querySelector('.subscribe__button').onclick = function (){
    let dateStart = document.querySelector('#start-date').value;
    const choiceDate = new Date(dateStart);
    let numDay = choiceDate.getDate();
    let dayWeek = days[choiceDate.getDay()];
    let monthYear = month[choiceDate.getMonth()];
    let year = choiceDate.getFullYear();

    let oneJan = new Date(choiceDate.getFullYear(),0,1);
    let numberOfDays = Math.floor((choiceDate - oneJan) / (24 * 60 * 60 * 1000));
    let weekNumber = Math.ceil(( choiceDate.getDay() + 1 + numberOfDays) / 7);
    if (dayWeek === undefined || numDay === undefined || monthYear === undefined || weekNumber === NaN || year === NaN) {
        let choice = document.querySelector('.subscribe__out').innerHTML = 'Enter value';
    } else {
    let choice = document.querySelector('.subscribe__out').innerHTML = `${dayWeek}, ${numDay} ${monthYear}, ${weekNumber} week ${year}`;
    }
}
//////появление кнопки на вверх страницы
window.addEventListener("scroll", (event) => {
    const arrow = document.getElementById('arrow');
    if (window.scrollY > 15) {
        arrow.style.display = 'block';
        return;
    }

    arrow.style.display = 'none';
})

